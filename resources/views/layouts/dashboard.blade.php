@extends('layouts.app')
 
@section('title', 'dashboard')

@section('content')
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 100%}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 500px;
    }
        
    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 100%) {
      .row.content {height: 500px;} 
    }
    
    @media only screen and (min-width: 60em) { /* 960px */
  .container {
    width: 100%;
    max-width: 100%;
  }
}

.well{
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#f5f5f5;
    border:1px solid #e3e3e3;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.05);
    box-shadow:inset 0 1px 1px rgba(0,0,0,.05)
}
</style>
</head>
<br><br>
<div class="row">
    <div class="col-12-sm">
        <div class="row">
        <div class="col-3-sm">
          <div class="well">
            <h4>Users</h4>
            <p><?php echo DB::table('users')->count();?></p> 
          </div>
        </div>
        <div class="col-3-sm">
          <div class="well">
            <h4>Pages</h4>
            <p>100 Million</p> 
          </div>
        </div>
        <div class="col-3-sm">
          <div class="well">
            <h4>Sessions</h4>
            <p><?php echo count(Session::all()) - 3; ?></p> 
          </div>
        </div>
        <div class="col-3-sm">
          <div class="well">
            <h4>Bounce</h4>
            <p>30%</p> 
          </div>
        </div>
      </div>
        <div class="row">
            <div class="col-12-sm">
                <div class="well" style="height: 350px;overflow-y: scroll;">
                <h4>News</h4>
                <p>Some text..</p>
              </div>  
            </div>
        </div>
      <div class="row">
        <div class="col-4-sm">
          <div class="well">
            <p>Text</p> 
            <p>Text</p> 
            <p>Text</p> 
          </div>
        </div>
        <div class="col-4-sm">
          <div class="well">
            <p>Text</p> 
            <p>Text</p> 
            <p>Text</p> 
          </div>
        </div>
        <div class="col-4-sm">
          <div class="well">
            <p>Text</p> 
            <p>Text</p> 
            <p>Text</p> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-8-sm">
          <div class="well">
            <p>Text</p> 
          </div>
        </div>
        <div class="col-4-sm">
          <div class="well">
            <p>Text</p> 
          </div>
        </div>
      </div>
    </div>
</div>


<script>
    
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
@endsection