@extends('layouts.app')
 
@section('title', 'friend')

@section('content')

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
</head>

<div id="main-container">
	<div id="sidebar">
            <div class="dropdown">
                <button onclick="myFunction()" class="dropbtn" style="left: 0;">Find some friends</button>
                <div id="myDropdown" class="dropdown-content" style="right: initial; left: 50;">
                      <input type="text" placeholder="Search.." id="myInput" oninput="filterFunction()">
                        
                    </div>
            </div>      
            <?php $user_id = Auth::user()->id;
                        $friends = DB::table('friends')
                                        ->join('users', 'friends.friend_id', '=', 'users.id')
                                        ->where('user_id', $user_id)
                                        ->select('users.*')
                                        ->get()->toArray();
                        $user = DB::table('users')->where('id', $user_id)->first();
                        foreach($friends as $friend){
                            ?>
                            <a href="friend/id"><?php echo $friend->surname?></a>
                           <?php 
                        }
            ?>
	</div>
    <div id="content">
        
    </div>
</div>



<script>
 $.get('/getprofile/' + '{{Auth::user()->id}}', function(response){
     user = response['user'];
     printProfile(user);
 });
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
    if(document.getElementById('user') !== null)
        document.getElementById('user').remove();
}

function filterFunction() {
    if(document.getElementById('user') !== null)
        document.getElementById('user').remove();
    var input, filter;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    
        $.get('/findFriend', { input: filter}, function(response) {
                if(document.getElementById('user') !== null)
                    document.getElementById('user').remove();
    
            if(response !== 1){
                
                if(document.getElementById('user') !== null)
                    document.getElementById('user').remove();
                
                response.forEach(function (element){
                    var a = document.createElement('a');
                    var linkText = document.createTextNode(element.name +"  "+ element.surname + ' - ' + element.username);
                    a.appendChild(linkText);
                    a.id   = 'user';
                    a.name = element.id;
                   a.href = "javascript:getProfileByID(" + a.name + ")";
                    a.class = ".special";
                    document.getElementById('myDropdown').appendChild(a);
                 });
            }
    });    
}

function getProfileByID(a){
    
    $.get("/getprofile/"+ a, function(response){
       if(response){
           user = response['user'];
           userInformation = response['userInformation'];
           printProfile(user, userInformation);
       }
    });

}


function printProfile(user, userInformation = null){
    var html = '<br><br><br><div class = "container center">';
    var userIdLogged = '{{Auth::user()->id}}';
    if(userInformation == null && user.id != userIdLogged){
        html += 'You are not yet friends, send him a friend request';
    }
    
        html += '<br><br><br><div class = "row">';
            html += '<div class = "col-12-sm center">';
                html += '<input class="field" type = "text" placeholder="' + user.username + '" readonly>';
            html += '</div>';
        html += '</div><br><br>';
    
        html += '<div class = "row">';
            html += '<div class = "col-12-sm center">';
                html += '<input class="field" type = "text" placeholder="' + user.name + '" readonly>';
            html += '</div>';
        html += '</div><br><br>';
    
        html += '<div class = "row">';
            html += '<div class = "col-12-sm center">';
                html += '<input class="field" type = "text" placeholder="' + user.surname + '" readonly>';
            html += '</div>';
        html += '</div><br><br>';
        
    
    if(user.id != userIdLogged){
        if(userInformation !== null){
            if(userInformation.confirmed == true){
                html += 'You are friends since: ' + userInformation.created_at;
            } else if(userInformation.confirmed == false && userInformation.friend_id == userIdLogged){
                html += '<a href="/acceptFriendRequest/' + userInformation.user_id + '"><div class="row"><div class="col-12-sm center"><input type = "submit" class = "button" value = "accept friend request"></div></div></a>';
            } else {                
                html += 'friendship request not yet confirmed';
            }
        } else {
            html += '<a href="/sendfriendrequest/' + user.id + '"><div class="row"><div class="col-12-sm center"><input type = "submit" class = "button" value = "send friend request"></div></div></a>';
        }
    }
    html +='</div>';
    document.getElementById('content').innerHTML = html;
    
}

</script>

<style type="text/css">

html {
	height: 100%;
}

body {
	margin: 0;
	height: 100%;
}

#main-container {
	display: table;
	width: 100%;
	height: 100%;
}

#sidebar {
	display: table-cell;
	width: 15%;
	vertical-align: top;
	background-color: red;
}

#sidebar a {
	display: block;
	padding: 10px;
	color: black;
	margin: 15px 0 0 0;
	text-decoration: none;
        transition-duration: 0.5s;
}

#sidebar a:hover {
	background-color: #d3e0e9;
	color: blue !important;
}

#content {
	display: table-cell;
	width: 85%;
	vertical-align: top;
	padding: 10px 0 0 10px;
}

#myInput {
    border-box: box-sizing;
/*    background-image: url('searchicon.png');*/
    background-position: 14px 12px;
    background-repeat: no-repeat;
    font-size: 16px;
    padding: 14px 20px 12px 45px;
    border: none;
    border-bottom: 1px solid #ddd;
}

.special{}

</style>
@endsection
