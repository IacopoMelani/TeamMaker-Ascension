 @extends('layouts.app')
 
 @section('title', 'welcome')

 @section('content')
 <div id='titolo' class='center'>
    <h1>Team Maker</h1>
    <div class="container center">
            <p>Your personal Team Maker application</p>
        <div class="row">
            <div class="col-6 right">
                <div id="login">
                    <a href="/login/view">
                        <input type="button" class="button" value="login" style='width: 125px;'>
                    </a>
                </div>
            </div>
            <div class="col-6 left">
                <div id="register">
                    <a href="/register/view">   
                        <input type="button" class="button" value="register" style='width: 125px;'>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
