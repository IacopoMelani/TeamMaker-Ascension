
@extends('layouts.app')
 
@section('title', 'profile')

@section('content')


<?php

$user = Auth::user();

?>


<div id='titolo' class='center'>
    <h1>Profile</h1>
    <div class="container center">
        <form action="/updateprofile" method="POST">
            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "username" placeholder="username: <?php echo $user->username ?>">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "name" placeholder="name: <?php echo $user->name ?>">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "surname" placeholder="surname: <?php echo $user->surname ?>">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "password" name = "oldPassword" placeholder="old password">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "password" name = "newPassword" placeholder="new password">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center">
                    <input type = "submit" class = "button" value = "Update">
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="<?php echo $user->id ?>">
        </form>
    </div>
</div>

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

@endsection