
<?php 
$user = Auth::user();
?>

    <div class="topnav">
        <div style="float:left;">
            <a class="active" href="/home">Home</a>
            <a href="#news">Team</a>
            <a href="/friend">Friends</a>
        </div>
        <div class="dropdown" style="float:right;">
            <button class="dropbtn"><?php echo $user->username; ?></button>
            <div class="dropdown-content">
                <a class="adrop" href="/profile">Personal</a>
                <a class="adrop" href="logout">Logout</a>
            </div>
        </div>
    </div>
    
