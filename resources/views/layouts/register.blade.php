@extends('layouts.app')
 
@section('title', 'register')

@section('content')
<br><br>
<div id='titolo' class='center'>
    <h1>Register</h1>
    <div class="container center">
        <form action="/register" method="POST">
            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "username" placeholder="Username">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "name" placeholder="Name">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "surname" placeholder="Surname">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "text" name = "email" placeholder="Email">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center" >
                    <input class="field" type = "password" name = "password" placeholder="Password">
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-12-sm center">
                    <input type = "submit" class = "button" value = "Sign in">
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
</div>
@endsection