@extends('layouts.app')
@section('title', 'login')

@section('content')
<br><br>
<div id='titolo' class='center'>
    <h1>Login</h1>
    <div class="container center">
        <form action="/login" method="POST">
        <div class="row">
            <div class="col-12-sm center" >
                <input class="field" type = "text" name = "email" placeholder="email">
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-12-sm center" >
                <input class="field" type = "password" name = "password" placeholder="Password">
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-12-sm center">
                <input type = "submit" class = "button" value = "Log in">
            </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>

    </div>
</div>

<script>
    
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
@endsection