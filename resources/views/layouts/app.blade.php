<html>
    <head>
        <title>Team Maker Ascension - @yield('title')</title>
        <link rel="stylesheet" href="<?php echo asset('css/common.css')?>" type="text/css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    </head>
    <body>
        @if($user = Auth::user())
            @include('layouts.navbar')
        @endif    
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>