<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('layouts/index');
});

Route::get('/login/view', function(){
    if($user = Auth::user()) {
        return redirect('/home');
    } else {
        return view('layouts/login');
    }
});

Route::get('/register/view', function(){
    if($user = Auth::user()) {
        Toastr::info("You are now log out");
        return redirect('/logout')->with("fromRegister", true);
    } else {
        return view('layouts/register');
    }
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    
    
    Route::get('/home', function(){
        return view('layouts/dashboard');
    });
    
    Route::get('/friend', function(){
        return view('layouts/friend');
    });
    
    Route::get('/profile', function(){
        return view('layouts/profile');
    });
    
    Route::post('/updateprofile', 'UserController@updateUser');
    
    Route::get('/getprofile/{id}', 'UserController@getProfileByID');

    Route::get('/findFriend', 'UserController@findByWords')->name('findfriend');
    
    Route::get('/sendfriendrequest/{id}', 'UserController@sendFriendRequest');
    
    Route::get('/acceptFriendRequest/{id}', 'UserController@acceptFriendRequest');
    
});