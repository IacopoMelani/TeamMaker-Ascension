<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Friend extends Model
{
    use SoftDeletes;
    
    protected $table = 'friends';
    
    protected $fillable = ['id', 'user_id', 'friend_id', 'confirmed'];
    
    
    
}
