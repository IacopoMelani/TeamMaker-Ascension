<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Friend;

class UserController extends Controller
{
    
    
    public function findByWords(Request $request){
     
        $data = $request->all();
        $input = filter_var($data['input'], FILTER_SANITIZE_STRING);
        if(trim($input) === ''){
            return -1;
        }
        $filters = explode(" ", $input);
        
        foreach($filters as $filter){
            $users = \DB::table('users')
                ->where('username', 'like', "%{$filter}%")
                ->orwhere('username', 'like', "%{$filter}")
                ->orwhere('username', 'like', "{$filter}%")
                ->orWhere('name', 'like', "%{$filter}%")
                ->orWhere('name', 'like', "%{$filter}")
                ->orWhere('name', 'like', "{$filter}%")
                ->orWhere('surname', 'like', "%{$filter}%")
                ->orWhere('surname', 'like', "%{$filter}")
                ->orWhere('surname', 'like', "{$filter}%")
                ->get()->toArray();
                
                if(!empty($users)){
                    return $users;
                }
        }
        return -1;
        
    }
    
    
    public function updateUser(Request $request){
        
        $request->session()->forget('message');
        $request->session()->forget('alert-type');
        
        $data = $request->all();
        $user = \Auth::user();

        $newUsername = isset($data['username']) ? $data['username'] : $user->username;
        $newName     = isset($data['name']) ? $data['name'] : $user->name;
        $newSurname  = isset($data['surname']) ? $data['surname'] : $user->surname;
        
        if(isset($data['newPassword']) && password_verify($data['oldPassword'], $user->password) ){
            
            $user->username = $newUsername;
            $user->name     = $newName;
            $user->surname  = $newSurname;
            $user->password = bcrypt($data['newPassword']);
            $user->save();
            
        } elseif(isset($data['newPassword']) && !password_verify($data['oldPassword'], $user->password)){    
            $notification = array(
                'message' => 'wrong password', 
                'alert-type' => 'error'
            );
            return redirect('/profile')->with($notification);
            
        } else{
            $user->username = $newUsername;
            $user->name     = $newName;
            $user->surname  = $newSurname;
            $user->save();
            
        }  
        $notification = array(
                'message' => 'profile updated', 
                'alert-type' => 'success'
        );
        return redirect('/profile')->with($notification);
    }
    
    public function getProfileByID(Request $request, $id){
        
        $result = array(
          'user'            => null,  
          'userInformation' => null,  
        );
        
        $data = $request->all();
        $loggedUser = \Auth::user();
        
        $user = \DB::table('users')->where('id', $id)->first();
        
        $userFriendInformation = \DB::table('friends')
                                    ->where('user_id'  , $loggedUser->id)
                                    ->where('friend_id', $user->id)
                                    ->orWhere('friend_id', $loggedUser->id)
                                    ->where('user_id'  , $user->id)
                                    ->first();
        $result['user']            = $user;
        $result['userInformation'] = $userFriendInformation != null ? $userFriendInformation : null;
        return $result;
    }
    
    public function sendFriendRequest(Request $request, $id){
        
        $userFriend = User::find($id);
        
        if($userFriend){
            $friend = new Friend();
            $friend->user_id = \Auth::user()->id;
            $friend->friend_id = $id;
            $friend->confirmed = FALSE;
            if($friend->save()){
                $notification = array(
                    'message' => 'Friend request successfully send', 
                    'alert-type' => 'success'
                )
                        ;
            } else {
                $notification = array(
                    'message' => 'Friend request failed', 
                    'alert-type' => 'error'
                );
            }
        } else {
           $notification = array(
                'message' => 'User not valid', 
                'alert-type' => 'error'
            ); 
        }
        return redirect('/friend')->with($notification);
    }
    
    public function acceptFriendRequest(Request $request, $id){
        
        $userFriendRequest = User::find($id);
        $user_id = \Auth::user()->id;
        if($userFriendRequest){
            $friendRequest = \DB::table('friends')->where('user_id', $id)->where('friend_id', $user_id)->first();
            if($friendRequest){

                $acceptedFriend = new Friend();
                $acceptedFriend->user_id = $user_id;
                $acceptedFriend->friend_id = $id;
                $acceptedFriend->confirmed = TRUE;
                if(\DB::table('friends')->where('id',$friendRequest->id)->update(['confirmed' => 1]) && $acceptedFriend->save()){
                    $notification = array(
                        'message' => "You are now friend of {$userFriendRequest->surname}", 
                        'alert-type' => 'success'
                    ); 
                } else {
                    $notification = array(
                        'message' => "Failed to confirm friend request", 
                        'alert-type' => 'error'
                    ); 
                }
            } else {
                $notification = array(
                    'message' => 'User not valid', 
                    'alert-type' => 'error'
                ); 
            }
        }
        $notification = array(
            'message' => 'User not valid', 
            'alert-type' => 'error'
        ); 
        return redirect('/friend')->with($notification);
    }
}
