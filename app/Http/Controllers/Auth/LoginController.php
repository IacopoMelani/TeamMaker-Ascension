<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\MessageBag;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Artdarek\Toastr\Facades\Toastr;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected function authenticated(Request $request, $user)
    {
        $notification = array(
                'message' => 'login success', 
                'alert-type' => 'success'
            );
        return redirect('/home')->with($notification);
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {

        if ( ! User::where('email', $request->email)->first() ) {
            $notification = array(
                'message' => 'wrong email', 
                'alert-type' => 'error'
            );
            return redirect('/login/view')->with($notification);
        }

        if ( ! User::where('email', $request->email)->where('password', bcrypt($request->password))->first() ) {

            $notification = array(
                'message' => 'wrong password', 
                'alert-type' => 'error'
            );
            return redirect('/login/view')->with($notification);
        }
    }
    
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $data = session()->all();
        $request->session()->invalidate();
        if(isset($data['fromRegister']))
            return redirect('/register/view');
        return redirect('/');
    }
    
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return redirect('/login/view');
    }
}
